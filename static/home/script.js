$(document).ready(function(){
    searchBook("Python Programming Language");
    $("#searchbook").change(function(){
        searchBook(document.getElementById("searchbook").value);
    });
});

function searchBook(toSearch) {
    $.getJSON(`https://www.googleapis.com/books/v1/volumes?q=${toSearch}`, function(result){
        let counter = 1;
        document.getElementsByTagName("table")[0].innerHTML = 
`
<thead>
<tr>
    <th scope="col">#</th>
    <th scope="col">Preview</th>
    <th scope="col">Title</th>
    <th scope="col">Authors</th>
    <th scope="col" class="text-center">Desc</th>
</tr>
</thead>
<tbody>
</tbody>
`;
        result.items.forEach(value => {
            let row = document.createElement("tr");

            let data0 = document.createElement("td");
            data0.innerText = counter;
            row.appendChild(data0);

            let data1 = document.createElement("td");
            data1.innerHTML = `<img src="${value.volumeInfo.imageLinks.thumbnail}" alt="">`;
            row.appendChild(data1);

            let data2 = document.createElement("td");
            data2.innerText = value.volumeInfo.title;
            row.appendChild(data2);
            
            let data3 = document.createElement("td");
            data3.innerText = value.volumeInfo.authors.join(", ");
            row.appendChild(data3);
            
            let data4 = document.createElement("td");
            data4.classList.add("text-center");
            data4.innerHTML =
`
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#openDesc${counter}">Open</button>
<div class="modal fade" id="openDesc${counter}" tabindex="-1" role="dialog" aria-labelledby="exampleOpenDesc${counter}" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleOpenDesc${counter}"> ${value.volumeInfo.title}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-left">
            ${value.volumeInfo.description ? value.volumeInfo.description : "Empty"}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
`;
            row.appendChild(data4);
            document.getElementsByTagName("tbody")[0].appendChild(row);
            ++counter;
        });
    });
}